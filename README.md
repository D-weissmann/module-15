# Module 15 - Citi Bike Analysis

#Overview 
This analysis of New York's Citi Bike program shows that it likely would be a good investment opportunity in Des Moines. It would alleviate traffic and creates repeat customers. 

#Results. 
This Analysis has 7 charts that can be seen [here](https://public.tableau.com/app/profile/david.a.weissmann/viz/DUData-DavidW-Module15/CitiBikeAnalysis?publish=yes):
 1. Start times of rides by time of day.
 2. Start times of rides by time of day and sex. 
 3. Use time by individual bike.
 4. Starting locations.
 5. Ride Length.
 6. Ride Length by Sex.
 7. Ending locations. 

 Further analysis can be found on the story. 

 #Overview 
 This analysis shows several benefits to potential investors and the city of Des Moines, such as the potential lessening of traffic during  peak times and that a majority of customers are subscribers which not only gives us valuable demographic data but also repeat customers. In addition it shows several ways we could be more efficient with bikes which could lead to less maintenance. It also shows that currently in New York they could increase profits by increasing female participation. 
